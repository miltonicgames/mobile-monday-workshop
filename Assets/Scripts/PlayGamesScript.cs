﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;

public class PlayGamesScript : MonoBehaviour
{
    #if (UNITY_ANDROID || (UNITY_IPHONE && !NO_GPGS))

    // Use this for initialization
    void Start() {
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();

        SignIn();
    }

    void SignIn() {
        Social.localUser.Authenticate(success => { });
    }

    // Achievements
    public static void UnlockAchievement(string id) {
        Social.ReportProgress(id, 100, success => { });
    }

    public static void IncrementAchievement(string id, int stepsToIncrement) {
        PlayGamesPlatform.Instance.IncrementAchievement(id, stepsToIncrement, success => { });
    }

    public static void ShowAchievementsUI() {
        Social.ShowAchievementsUI();
    }

    // Leaderboards
    public static void AddScoreToLeaderboard(string leaderboardId, long score) {
        Social.ReportScore(score, leaderboardId, success => { });
    }

    public static void ShowLeaderboardsUI() {
        Social.ShowLeaderboardUI();
    }
    #endif
}
