﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayField : MonoBehaviour {
    [SerializeField] private GamePiece piecePrefab;
    [SerializeField] private UnityEngine.UI.Text countDownText;
    [SerializeField] private UnityEngine.UI.Text scoreText;

    [SerializeField] private int scoreMultiplyer;
    [SerializeField] private int scoreBase;

    private GamePiece[,] pieces;
    private bool[] currentValidColors;
    private float currentTime = 30;
    private int lastTouchCount, score, totalPops, highestCombo, failedPops;
    private float totalTime;

    private Animator anim;

    private const float TILE_SIZE = 1.2f;
    /// <summary>
    /// Runs when the scene has finished loading
    /// </summary>
    void Awake() {
        anim = GetComponent<Animator>();

        //Create and initialize all the game pieces
        pieces = new GamePiece[GameController.instance.MapData.mapWidth, GameController.instance.MapData.mapHeight];
        for(int i = 0; i < pieces.GetLength(0); i++)
            for(int j = 0; j < pieces.GetLength(1); j++) {
                pieces[i, j] = Instantiate(piecePrefab, new Vector3(i * TILE_SIZE, -j * TILE_SIZE, 0), Quaternion.identity);
                pieces[i, j].Init(new Vector2Int(i, j), this, GameController.instance.MapData.fieldData[(j * pieces.GetLength(0)) + i]);

            }

        //Create the starting board, do until a working board has been found
        do {
            for(int i = 0; i < pieces.GetLength(0); i++)
                for(int j = 0; j < pieces.GetLength(1); j++)
                    SetPiece(ref pieces[i, j]);
        }
        while(!CheckSanity());

        //Keep trapcs of current valid color
        currentValidColors = new bool[GameController.instance.MapData.nrOfColors];
        ResetCurrentValidColors();

        //Center the camere on the tiles and zoom out
        int centerView = Mathf.Max(GameController.instance.MapData.mapHeight + 1, GameController.instance.MapData.mapWidth);
        Camera.main.transform.position = new Vector3((TILE_SIZE / 2) * GameController.instance.MapData.mapWidth - (TILE_SIZE / 2), -(TILE_SIZE / 2) * GameController.instance.MapData.mapHeight, -10);
        Camera.main.orthographicSize = centerView;

        if(GameController.instance.MapData.useTimer)
            StartCoroutine(TimeCountdown());
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="piece"></param>
    public void SetPiece(ref GamePiece piece) {
        bool[] colors = new bool[GameController.instance.MapData.nrOfColors];

        switch(piece.Type) {
            case MapData.FieldData.Empty:
                return;
            case MapData.FieldData.Rand:
                GetRandomColors(ref colors);
                break;
            case MapData.FieldData.Col1:
                colors[0] = true;
                break;
            case MapData.FieldData.Col2:
                colors[1] = true;
                break;
            case MapData.FieldData.Col3:
                colors[1] = true;
                break;
            case MapData.FieldData.Col4:
                colors[1] = true;
                break;
            case MapData.FieldData.Col5:
                colors[1] = true;
                break;
            case MapData.FieldData.Col1_Col2:
                colors[0] = true;
                colors[1] = true;
                break;
        }

        bool addClock = false;
        bool addBomb = false;
        int randomChance = Random.Range(0, 100);

        if(randomChance > 90 && GameController.instance.MapData.hasClocks)
            addClock = true;
        else if(randomChance < 5 && GameController.instance.MapData.hasBombs)
            addBomb = true;

        piece.SetPiece(colors, addBomb, addClock);
    }

    void GetRandomColors(ref bool[] colors) {
        int nrOfColors = 1;

        int randomNrOfColors = Random.Range(0, GameController.instance.MapData.singleColorSpawnRate + GameController.instance.MapData.dualColorSpawnRate + GameController.instance.MapData.tripleColorSpawnRate + GameController.instance.MapData.quadColorSpawnRate);
        if(randomNrOfColors > GameController.instance.MapData.singleColorSpawnRate + GameController.instance.MapData.dualColorSpawnRate + GameController.instance.MapData.tripleColorSpawnRate)
            nrOfColors = 4;
        else if(randomNrOfColors > GameController.instance.MapData.singleColorSpawnRate + GameController.instance.MapData.dualColorSpawnRate)
            nrOfColors = 3;
        else if(randomNrOfColors > GameController.instance.MapData.singleColorSpawnRate)
            nrOfColors = 2;
        else
            nrOfColors = 1;

        for(int i = 0; i < nrOfColors;) {
            int newRandom = Random.Range(0, GameController.instance.MapData.nrOfColors);
            if(colors[newRandom] == false) {
                colors[newRandom] = true;
                i++;
            }
        }
    }

    void Update() {
#if((!UNITY_ANDROID && !UNITY_IPHONE) || UNITY_EDITOR)
        if(Input.GetMouseButtonUp(0))
            ScorePieces();
#endif
#if ((UNITY_ANDROID || UNITY_IPHONE) && !UNITY_EDITOR)
            if(Input.touchCount == 0 && lastTouchCount != 0)
                ScorePieces();

            lastTouchCount = Input.touchCount;
#endif
    }

    public void CheckAdjacantePieces(bool[] newPieceColors) {
        //Update current valid Colors
        for(int i = 0; i < newPieceColors.Length; i++) {
            if(currentValidColors[i] == true && newPieceColors[i] == false)
                currentValidColors[i] = false;
        }

        //Update current valid tiles based on valid colors
        for(int i = 0; i < pieces.GetLength(0); i++)
            for(int j = 0; j < pieces.GetLength(1); j++) {
                if(pieces[i, j].IsSelected)
                    continue;

                pieces[i, j].IsValid = false;

                if(i > 0) {
                    if(pieces[i - 1, j].IsSelected)
                        pieces[i, j].SetValid(currentValidColors);
                }
                if(j > 0) {
                    if(pieces[i, j - 1].IsSelected)
                        pieces[i, j].SetValid(currentValidColors);
                }

                if(i < pieces.GetLength(0) - 1) {
                    if(pieces[i + 1, j].IsSelected)
                        pieces[i, j].SetValid(currentValidColors);
                }

                if(j < pieces.GetLength(1) - 1) {
                    if(pieces[i, j + 1].IsSelected)
                        pieces[i, j].SetValid(currentValidColors);
                }
            }
    }

    /// <summary>
    /// Whenever you score a group of pieces
    /// </summary>
    private void ScorePieces() {
        List<GamePiece> selectedPieces = GetSelectedPieces();
        ResetCurrentValidColors();

        //Have we selected 3 or more pieces?
        if(selectedPieces.Count < 3) {
            failedPops++;
            return;
        }

        //Check if extra time
        for(int i = 0; i < selectedPieces.Count; i++) {
            if(selectedPieces[i].HaveClock)
                currentTime += 5;
        }

        // Stats
        totalPops += selectedPieces.Count;

        if(selectedPieces.Count > highestCombo)
            highestCombo = selectedPieces.Count;

        // Calculating and Adding Score
        score += ((scoreMultiplyer * selectedPieces.Count) * (scoreMultiplyer * selectedPieces.Count)) + scoreBase;
        scoreText.text = "Score: " + score;

        //If the map goal is number of tilespop and we reach that goal
        if(GameController.instance.MapData.goal == MapData.Goal.TilePopGoal && totalPops >= GameController.instance.MapData.goalLimit) {
            EndGame();
        }

        //If the map goal is number of tilespop combo and we reach that goal
        else if(GameController.instance.MapData.goal == MapData.Goal.TileComboGoal && highestCombo >= GameController.instance.MapData.goalLimit) {
            EndGame();
        }

        //If the map goal is score and we reach that goal
        else if(GameController.instance.MapData.goal == MapData.Goal.ScoreGoal && score >= GameController.instance.MapData.goalLimit) {
            EndGame();
        }

        for(int i = 0; i < selectedPieces.Count; i++) {
            pieces[selectedPieces[i].Position.x, selectedPieces[i].Position.y].Pop();
        }
        do {
            for(int i = 0; i < selectedPieces.Count; i++) {
                SetPiece(ref pieces[selectedPieces[i].Position.x, selectedPieces[i].Position.y]);
            }
        } while(!CheckSanity());
    }

    private List<GamePiece> GetSelectedPieces() {
        List<GamePiece> selectedPieces = new List<GamePiece>();

        //Check if the selection is valid
        for(int i = 0; i < pieces.GetLength(0); i++)
            for(int j = 0; j < pieces.GetLength(1); j++) {
                if(pieces[i, j] == null)
                    continue;

                if(pieces[i, j].IsSelected)
                    selectedPieces.Add(pieces[i, j]);

                pieces[i, j].DeSelect();
            }

        return selectedPieces;
    }

    public void MoveReset() {
        for(int i = 0; i < pieces.GetLength(0); i++)
            for(int j = 0; j < pieces.GetLength(1); j++)
                pieces[i, j].DeSelect();

        ResetCurrentValidColors();
    }

    private void ResetCurrentValidColors() {
        for(int i = 0; i < currentValidColors.Length; i++)
            currentValidColors[i] = true;
    }

    private bool CheckSanity() {
        for(int i = 0; i < pieces.GetLength(0); i++)
            for(int j = 0; j < pieces.GetLength(1); j++) {
                bool[] color = pieces[i, j].Colors;

                for(int k = 0; k < color.Length; k++) {
                    int adjCheck = 0;

                    if(i > 0) {
                        if(pieces[i - 1, j].Colors[k] == true && color[k] == true)
                            adjCheck++;
                    }
                    if(j > 0) {
                        if(pieces[i, j - 1].Colors[k] == true && color[k] == true)
                            adjCheck++;
                    }

                    if(i < pieces.GetLength(0) - 1) {
                        if(pieces[i + 1, j].Colors[k] == true && color[k] == true)
                            adjCheck++;
                    }

                    if(j < pieces.GetLength(1) - 1) {
                        if(pieces[i, j + 1].Colors[k] == true && color[k] == true)
                            adjCheck++;
                    }

                    if(adjCheck > 1) {
                        return true;
                    }
                }
            }

        Debug.LogWarning("Invalid Playfield");
        return false;
    }

    private IEnumerator TimeCountdown() {
        while(currentTime > 0) {
            yield return new WaitForSeconds(0.1f);
            currentTime -= 0.1f;
            totalTime += 0.1f;

            if(currentTime > 99)
                currentTime = 99;

            countDownText.text = "Time: " + (int)currentTime;
        }
        EndGame();
    }

    public void EndGame() {
        // Transfer performance data to the game over scene.
        GameController.instance.LastScore = score;
        GameController.instance.TotalTime = totalTime;
        GameController.instance.TotalPops = totalPops;
        GameController.instance.HighestCombo = highestCombo;
        GameController.instance.FailedPops = failedPops;


        // Load Next Level if there is One
        if(GameController.instance.MapData.nextMap != null)
            GameController.instance.LoadScene(1, GameController.instance.MapData.nextMap);
        else
            GameController.instance.LoadScene(2);
    }
}