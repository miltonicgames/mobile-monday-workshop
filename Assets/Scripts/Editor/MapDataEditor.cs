﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MapData))]
public class MapDataEditor : Editor
{
    public override void OnInspectorGUI()
    {
        ///We store all changes to temp variabels so that we can serialize them later (otherwise changes in the inspector might not always save)
        MapData myTarget = (MapData)target;
        string mapNameSerialized = myTarget.mapName;
        MapData.Goal goalSerialized = myTarget.goal;
        bool useTimerSerialized = myTarget.useTimer;
        int goalLimitSerialized = myTarget.goalLimit;
        int nrOfColorsSerialized = myTarget.nrOfColors;
        int singleColorSpawnRateSerialized = myTarget.singleColorSpawnRate;
        int dualColorSpawnRateSerialized = myTarget.dualColorSpawnRate;
        int tripleColorSpawnRateSerialized = myTarget.tripleColorSpawnRate;
        int quadColorSpawnRateSerialized = myTarget.quadColorSpawnRate;
        bool hasBombsSerialized = myTarget.hasBombs;
        bool hasClocksSerialized = myTarget.hasClocks;
        int mapHeightSerialized = myTarget.mapHeight;
        int mapWidthSerialized = myTarget.mapWidth;
        MapData nextMapSerialized = myTarget.nextMap;

        GUILayout.BeginArea(new Rect(16, 48, Screen.width - 32, 260));
        mapNameSerialized = EditorGUILayout.TextField("Map Name: ", myTarget.mapName);
        goalSerialized = (MapData.Goal)EditorGUILayout.EnumPopup("Map Goal",myTarget.goal);

        if (myTarget.goal == MapData.Goal.ScoreGoal || myTarget.goal == MapData.Goal.TilePopGoal || myTarget.goal == MapData.Goal.TileComboGoal)
            goalLimitSerialized = EditorGUILayout.IntSlider("Goal Limit:", myTarget.goalLimit, 1, 1000000);

        useTimerSerialized = EditorGUILayout.Toggle("Use Timer: ", myTarget.useTimer);
        nrOfColorsSerialized = EditorGUILayout.IntSlider("# of Colors:", myTarget.nrOfColors, 2, 5);
        singleColorSpawnRateSerialized = EditorGUILayout.IntSlider("Single Color Spawn Rate:", myTarget.singleColorSpawnRate, 1, 100);
        dualColorSpawnRateSerialized = EditorGUILayout.IntSlider("Dual Color Spawn Rate:", myTarget.dualColorSpawnRate, 0, 100);

        if (myTarget.nrOfColors >= 3)
            tripleColorSpawnRateSerialized = EditorGUILayout.IntSlider("Tripple Color Spawn Rate:", myTarget.tripleColorSpawnRate, 0, 100);

        if (myTarget.nrOfColors >= 4)
            quadColorSpawnRateSerialized = EditorGUILayout.IntSlider("Quad Color Spawn Rate:", myTarget.quadColorSpawnRate, 0, 100);

        hasBombsSerialized = EditorGUILayout.Toggle("Bombs: ", myTarget.hasBombs);
        hasClocksSerialized = EditorGUILayout.Toggle("Clocks: ", myTarget.hasClocks);

        nextMapSerialized = EditorGUILayout.ObjectField("Next Map: ", myTarget.nextMap, typeof(MapData), false) as MapData;

        mapHeightSerialized = EditorGUILayout.IntSlider("Map Height", myTarget.mapHeight, 1, 10);
        mapWidthSerialized = EditorGUILayout.IntSlider("Map Width", myTarget.mapWidth, 1, 10);
        GUILayout.EndArea();

        SerializedObject obj = new SerializedObject(target);
        obj.FindProperty("mapName").stringValue = mapNameSerialized;
        obj.FindProperty("nrOfColors").intValue = nrOfColorsSerialized;
        obj.FindProperty("useTimer").boolValue = useTimerSerialized;
        obj.FindProperty("singleColorSpawnRate").intValue = singleColorSpawnRateSerialized;
        obj.FindProperty("dualColorSpawnRate").intValue = dualColorSpawnRateSerialized;
        obj.FindProperty("tripleColorSpawnRate").intValue = tripleColorSpawnRateSerialized;
        obj.FindProperty("quadColorSpawnRate").intValue = quadColorSpawnRateSerialized;
        obj.FindProperty("hasBombs").boolValue = hasBombsSerialized;
        obj.FindProperty("hasClocks").boolValue = hasClocksSerialized;
        obj.FindProperty("goal").enumValueIndex = (int)goalSerialized;
        obj.FindProperty("goalLimit").intValue = goalLimitSerialized;
        obj.FindProperty("nextMap").objectReferenceValue = nextMapSerialized;  

        if (mapHeightSerialized != myTarget.mapHeight || mapWidthSerialized != myTarget.mapWidth || myTarget.fieldData == null)
        {
            obj.FindProperty("mapHeight").intValue = mapHeightSerialized;
            obj.FindProperty("mapWidth").intValue = mapWidthSerialized;
            obj.FindProperty("fieldData").arraySize = mapHeightSerialized * mapWidthSerialized;
        }

        obj.ApplyModifiedPropertiesWithoutUndo();

        GUILayout.BeginArea(new Rect((Screen.width / 2) - (24 * myTarget.mapWidth), 332, Screen.width - 32, 20 * myTarget.mapHeight));

        for (int i = 0; i < myTarget.mapHeight; i++)
        {
            EditorGUILayout.BeginHorizontal(GUILayout.Width(24 * myTarget.mapWidth));

            for (int j = 0; j < myTarget.mapWidth; j++)
                myTarget.fieldData[(i * myTarget.mapWidth) + j] = (MapData.FieldData)EditorGUILayout.EnumPopup(myTarget.fieldData[(i * myTarget.mapWidth) + j]);

            GUILayout.EndHorizontal();
        }
        GUILayout.EndArea();

        GUILayout.BeginArea(new Rect((Screen.width / 2) - 48, 352 + (20 * myTarget.mapHeight), 196, 32));

        GUILayout.BeginHorizontal();

            if (GUILayout.Button("Empty All"))
            {
                for (int i = 0; i < myTarget.mapHeight; i++)
                    for (int j = 0; j < myTarget.mapWidth; j++)
                        myTarget.fieldData[(i * myTarget.mapWidth) + j] = MapData.FieldData.Empty;
            }

            else if (GUILayout.Button("Random All"))
            {
                for (int i = 0; i < myTarget.mapHeight; i++)
                    for (int j = 0; j < myTarget.mapWidth; j++)
                        myTarget.fieldData[(i * myTarget.mapWidth) + j] = MapData.FieldData.Rand;
            }

        GUILayout.EndHorizontal();

        GUILayout.EndArea();
    }
}