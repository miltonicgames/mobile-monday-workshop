﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverController : MonoBehaviour {

    [SerializeField] private UnityEngine.UI.Text scoreText;
    [SerializeField] private UnityEngine.UI.Text timeText;
    [SerializeField] private UnityEngine.UI.Text comboText;
    [SerializeField] private UnityEngine.UI.Text popsText;
    [SerializeField] private UnityEngine.UI.Text scorePerTimeText;
    [SerializeField] private UnityEngine.UI.Text failedMatchText;

    public void restartGame()
    {
        GameController.instance.LoadScene(1);
    }

    public void mainMenu()
    {
        GameController.instance.LoadScene(0);
    }

    void Awake()
    {
        scoreText.text = "Score: " + GameController.instance.LastScore;
        timeText.text = "Total Time Played: " + (int)GameController.instance.TotalTime;
        comboText.text = "Highest Pops at Once: " + GameController.instance.HighestCombo;
        popsText.text = "Total Number of Pops: " + GameController.instance.TotalPops;
        scorePerTimeText.text = "Average Score over Time: " + (GameController.instance.LastScore / GameController.instance.TotalTime);
        failedMatchText.text = "Number of Failed Matches: " + GameController.instance.FailedPops;
    }
}