﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour {

    [SerializeField] private GameObject[] subMenus;
    [SerializeField] private UnityEngine.UI.Text colorModeText;

    public Slider musicSlider;
    public Slider sfxSlider;

    public void openMenu(int activeSubMenu) {
        foreach(GameObject menu in subMenus) {
            menu.SetActive(false);
        }

        subMenus[activeSubMenu].SetActive(true);
    }

    public void ChangeMusicVolume(float sliderValue) {
        GameController.instance.MusicVolume = sliderValue;
    }

    public void ChangeSfxVolume(float sliderValue) {
        GameController.instance.SfxVolume = sliderValue;
    }

    public void ChangeColorMode(int modifier) {
        GameController.instance.ColorMode = GameController.instance.ColorMode + modifier;
        UpdateColorOptionText();
    }

    // Use this for initialization
    public void StartGame(MapData mapdata) {
        GameController.instance.LoadScene(1, mapdata);
    }

    private void UpdateColorOptionText() {
        switch(GameController.instance.ColorMode) {
            case 0:
                colorModeText.text = "Default";
                break;
            case 1:
                colorModeText.text = "Autumn";
                break;
            case 2:
                colorModeText.text = "Spring";
                break;
            case 3:
                colorModeText.text = "Deuteranopia";
                break;
            case 4:
                colorModeText.text = "Protanopia";
                break;
            case 5:
                colorModeText.text = "Tritanopia";
                break;
        }
    }

    public void AboutUsHyperlink() {
        Application.OpenURL("http://mostlyharmlessgames.se");
    }

    private void Awake() {
        UpdateColorOptionText();
        //musicSlider.value = GameController.instance.MusicVolume;
        //sfxSlider.value = GameController.instance.SfxVolume;


    }
}
