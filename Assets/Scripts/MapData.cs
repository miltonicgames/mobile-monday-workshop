﻿using UnityEngine;

// This object is used to store different kinds of Map Data
// E.g. "4x4", "5x5", "Circle", Etc.

[CreateAssetMenu(fileName = "MapData", menuName = "MapData", order = 1)]
public class MapData : ScriptableObject 
{
    public enum Goal {TimeLimit, ScoreGoal, TilePopGoal, TileComboGoal}
    public Goal goal;
    public enum FieldData {Empty, Rand, Col1, Col2, Col3, Col4, Col5, Col1_Col2}
    public int goalLimit = 1;
    public bool useTimer;
    public string mapName = "New Map";
    public int nrOfColors = 4;
    public bool hasBombs = false;
    public bool hasClocks = false;
    public int singleColorSpawnRate = 100;
    public int dualColorSpawnRate = 0;
    public int tripleColorSpawnRate = 0;
    public int quadColorSpawnRate = 0;
    public int mapHeight = 1;
    public int mapWidth = 1;
    public FieldData[] fieldData;
    public MapData nextMap;
}