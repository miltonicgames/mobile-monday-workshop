﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class AnalyticsEvents : MonoBehaviour {

    public void ReportGameTime(int gameTime) {
        AnalyticsEvent.Custom("Game_Time", new Dictionary<string, object>
        {
        { "game_time", gameTime },
        { "time_elapsed", Time.timeSinceLevelLoad }
    });
    }
}
