﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Main Game Controller
/// </summary>
public class GameController : MonoBehaviour {
    public static GameController instance = null;

    // Gameplay
    [SerializeField] private MapData mapData;
    public MapData MapData {
        get {
            return mapData;
        }
    }

    // Color Modes
    enum ColorBlindMode { Default, Autumn, Spring, Deuteranopia, Protanopia, Tritanopia };
    private ColorBlindMode colorBlindMode;
    /// <summary>
    /// Set and return colorMode as int
    /// </summary>
    /// 

    public int ColorMode {
        get { return (int)colorBlindMode; }
        set { if(value <= 5 && value >= 0) colorBlindMode = (ColorBlindMode)value; }
    }

    public Color GetColor(int color) {
        if(colorBlindMode == ColorBlindMode.Default) {
            if(color == 0)
                return new Color32(73, 10, 61, 255);
            else if(color == 1)
                return new Color32(189, 21, 80, 255);
            else if(color == 2)
                return new Color32(233, 127, 2, 255);
            else if(color == 3)
                return new Color32(248, 202, 0, 255);
            else if(color == 4)
                return new Color32(138, 155, 15, 255);
            else if(color == 5)
                return Color.grey;
            else {
                Debug.LogWarning("Incorrect number of Colors!");
                return new Color(1, 0, 1);
            }
        }
        else if(colorBlindMode == ColorBlindMode.Autumn) {
            if(color == 0)
                return new Color32(236, 208, 120, 255);
            else if(color == 1)
                return new Color32(217, 91, 67, 255);
            else if(color == 2)
                return new Color32(192, 41, 66, 255);
            else if(color == 3)
                return new Color32(84, 36, 55, 255);
            else if(color == 4)
                return new Color32(83, 119, 122, 255);
            else if(color == 5)
                return Color.grey;
            else {
                Debug.LogWarning("Incorrect number of Colors!");
                return new Color(1, 0, 1);
            }
        }
        else if(colorBlindMode == ColorBlindMode.Spring) {
            if(color == 0)
                return new Color32(85, 98, 112, 255);
            else if(color == 1)
                return new Color32(78, 205, 196, 255);
            else if(color == 2)
                return new Color32(199, 244, 100, 255);
            else if(color == 3)
                return new Color32(255, 107, 107, 255);
            else if(color == 4)
                return new Color32(196, 77, 88, 255);
            else if(color == 5)
                return Color.grey;
            else {
                Debug.LogWarning("Incorrect number of Colors!");
                return new Color(1, 0, 1);
            }
        }
        else if(colorBlindMode == ColorBlindMode.Deuteranopia) {
            if(color == 0)
                return new Color32(0, 146, 146, 255);
            else if(color == 1)
                return new Color32(0, 109, 219, 255);
            else if(color == 2)
                return new Color32(109, 182, 255, 255);
            else if(color == 3)
                return new Color32(146, 73, 0, 255);
            else if(color == 4)
                return new Color32(36, 255, 36, 255);
            else if(color == 5)
                return Color.grey;
            else {
                Debug.LogWarning("Incorrect number of Colors!");
                return new Color(1, 0, 1);
            }
        }
        else if(colorBlindMode == ColorBlindMode.Protanopia) {
            if(color == 0)
                return new Color32(255, 109, 182, 255);
            else if(color == 1)
                return new Color32(0, 109, 219, 255);
            else if(color == 2)
                return new Color32(109, 182, 255, 255);
            else if(color == 3)
                return new Color32(146, 73, 0, 255);
            else if(color == 4)
                return new Color32(36, 255, 36, 255);
            else if(color == 5)
                return Color.grey;
            else {
                Debug.LogWarning("Incorrect number of Colors!");
                return new Color(1, 0, 1);
            }
        }
        else if(colorBlindMode == ColorBlindMode.Tritanopia) {
            if(color == 0)
                return new Color32(0, 146, 146, 255);
            else if(color == 1)
                return new Color32(255, 109, 182, 255);
            else if(color == 2)
                return new Color32(0, 73, 73, 255);
            else if(color == 3)
                return new Color32(146, 0, 0, 255);
            else if(color == 4)
                return new Color32(36, 255, 36, 255);
            else if(color == 5)
                return Color.grey;
            else {
                Debug.LogWarning("Incorrect number of Colors!");
                return new Color(1, 0, 1);
            }
        }

        else {
            Debug.LogWarning("Incorrect colorBlindMode setting!");
            return new Color(1, 0, 1);
        }
    }

    // Music Settings
    private float musicVolume;
    public float MusicVolume {
        set {
            musicVolume = value;
        }
        get {
            return musicVolume;
        }
    }
    private float sfxVolume;
    public float SfxVolume {
        set {
            sfxVolume = value;
        }
        get {
            return sfxVolume;
        }
    }

    // Highscore

    private int lastScore;
    public int LastScore {
        set {
            lastScore = value;
        }
        get {
            return lastScore;
        }

    }

    private float totalTime;
    public float TotalTime {
        set {
            totalTime = value;
        }
        get {
            return totalTime;
        }
    }

    private int totalPops;
    public int TotalPops {
        set {
            totalPops = value;
        }
        get {
            return totalPops;
        }
    }

    private int highestCombo;
    public int HighestCombo {
        set {
            highestCombo = value;
        }
        get {
            return highestCombo;
        }
    }

    private int failedPops;
    public int FailedPops {
        set {
            failedPops = value;
        }
        get {
            return failedPops;
        }
    }

    private int highScore;
    public int HighScore {
        set {
            highScore = value;
        }
        get {
            return highScore;
        }
    }

    private void Awake() {
        if(instance == null) {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if(instance != null)
            Destroy(gameObject);
    }

    public void LoadScene(int scene, MapData mapData = null) {
        if(mapData != null)
            this.mapData = mapData;

        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }

}
