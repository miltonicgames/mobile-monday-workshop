﻿using System;
using System.Collections;
using UnityEngine;

public class GamePiece : MonoBehaviour {
    [SerializeField] private MeshRenderer bgMeshRenderer;
    [SerializeField] private MeshRenderer fgMeshRenderer;
    [SerializeField] private TextMesh bombText;

    private Vector2Int position;

    [SerializeField] private Texture[] tileTextures;
    [SerializeField] private Texture[] tileOverlay;

    public Vector2Int Position { get { return position; } }

    private bool[] colors;
    public bool[] Colors { get { return colors; } }
    private MapData.FieldData type;
    public MapData.FieldData Type { get { return type; } }
    private bool haveBomb;
    private bool haveClock;
    public bool HaveClock { get { return haveClock; } }

    private bool isValid = true;
    public bool IsValid { get { return isValid; } set { isValid = value; } }
    private bool isSelected;
    public bool IsSelected { get { return isSelected; } }
    private PlayField playfield;

    private Animator anim;

    private float bombCountdown = 9;
    private const float OUTLINE_SIZE = 1.0f;

    public void Init(Vector2Int position, PlayField playfield, MapData.FieldData type) {
        this.position = position;
        this.playfield = playfield;
        this.type = type;

        colors = new bool[GameController.instance.MapData.nrOfColors];

        if(type == MapData.FieldData.Empty)
            bgMeshRenderer.gameObject.SetActive(false);

    }

    public void SetPiece(bool[] colors, bool haveBomb, bool haveClock) {

        this.colors = colors;
        this.haveBomb = haveBomb;
        this.haveClock = haveClock;

        //Reset the bomb counter
        bombText.gameObject.SetActive(false);
        StopAllCoroutines();

        if(haveClock)
            fgMeshRenderer.material.mainTexture = tileOverlay[1];
        else if(haveBomb) {
            bombText.gameObject.SetActive(true);
            bombText.offsetZ = -2;
            bombCountdown = 9;
            StartCoroutine(BombTimeCountdown());
            fgMeshRenderer.material.mainTexture = tileOverlay[2];
        }
        else
            fgMeshRenderer.material.mainTexture = tileOverlay[0];

        int nrOfColors = 0;

        for(int i = 0; i < colors.Length; i++) {
            if(colors[i] == true) {
                nrOfColors++;
                bgMeshRenderer.material.SetColor("_Color" + nrOfColors, GameController.instance.GetColor(i));
            }
        }

        bgMeshRenderer.material.SetTexture("_MainTex", tileTextures[nrOfColors - 1]);
    }

    private void Start() {
        anim = GetComponent<Animator>();
    }

    private void Update() {

    }

    private void OnMouseOver() {
        if(!isValid)
            playfield.MoveReset();

#if ((!UNITY_ANDROID && !UNITY_IPHONE) || UNITY_EDITOR)
        if(!Input.GetMouseButtonDown(0))
            return;
#endif

        Select();
    }
    /// <summary>
    /// Also works on touch controls
    /// </summary>
    void OnMouseEnter() {
        if(!isValid)
            playfield.MoveReset();

#if ((!UNITY_ANDROID && !UNITY_IPHONE) || UNITY_EDITOR)
        if(!Input.GetMouseButton(0))
            return;
#endif
        Select();
    }

    public void Select() {
        bgMeshRenderer.material.SetFloat("_OutlineWidth", OUTLINE_SIZE);
        isSelected = true;
        playfield.CheckAdjacantePieces(colors);
        anim.SetBool("isSelected", true);
    }

    public void DeSelect() {
        bgMeshRenderer.material.SetFloat("_OutlineWidth", 0.0f);
        isSelected = false;
        isValid = true;
        anim.SetBool("isSelected", false);
    }

    public void Pop() {
        anim.SetTrigger("tilePopped");
    }

    public void SetValid(bool[] colorCheck) {
        for(int i = 0; i < colors.Length; i++) {
            if(colors[i] == true && colorCheck[i] == true)
                isValid = true;
        }
    }

    private IEnumerator BombTimeCountdown() {
        while(bombCountdown > 0) {
            yield return new WaitForSeconds(0.1f);
            bombCountdown -= 0.1f;
            bombText.text = "" + (int)bombCountdown;
        }

        if(bombCountdown <= 0)
            playfield.EndGame();
    }
}