﻿Shader "Outlined/Triangle"
{
	Properties
	{
		_Color1("Color 1", Color) = (0.5,0.5,0.5,1)
		_Color2("Color 2", Color) = (0.5,0.5,0.5,1)
		_Color3("Color 3", Color) = (0.5,0.5,0.5,1)
		_Color4("Color 4", Color) = (0.5,0.5,0.5,1)
		_MainTex("Texture", 2D) = "white" {}
		_OutlineColor("Outline color", Color) = (0,0,0,1)
		_OutlineWidth("Outlines width", Range(0.0, 2.0)) = 1.1
	}

		CGINCLUDE
#include "UnityCG.cginc"

		struct appdata
	{
		float4 vertex : POSITION;
	};

	struct v2f
	{
		float4 pos : POSITION;
	};

	uniform float _OutlineWidth;
	uniform float4 _OutlineColor;
	uniform sampler2D _MainTex;
	uniform float4 _Color1;
	uniform float4 _Color2;
	uniform float4 _Color3;
	uniform float4 _Color4;

	ENDCG

		SubShader
	{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" }

		Pass //Outline
	{
		ZWrite Off
		Cull Back
		Blend SrcAlpha OneMinusSrcAlpha
		CGPROGRAM

	#pragma vertex vert
	#pragma fragment frag

	v2f vert(appdata v)
	{
		appdata original = v;
		v.vertex.xyzw += _OutlineWidth * normalize(v.vertex.xyzw);
		v.vertex.x -= _OutlineWidth / 4;
		v.vertex.y += _OutlineWidth / 4;

		v2f o;
		o.pos = UnityObjectToClipPos(v.vertex);
		return o;

	}

	half4 frag(v2f i) : COLOR
	{
		return _OutlineColor;
	}

		ENDCG
	}

		Tags{ "Queue" = "Geometry" }

		CGPROGRAM
	#pragma surface surf Lambert

		struct Input {
		float2 uv_MainTex;
	};

	void surf(Input IN, inout SurfaceOutput o) 
	{
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex);

		if (c.g == 0 && c.b == 0 && c.a == 1)
			o.Albedo = lerp(c.rgb, _Color1, c.r);

		if(c.r == 0 && c.b == 0 && c.a == 1)
			o.Albedo = lerp(c.rgb, _Color2, c.g);

		if (c.r == 0 && c.g == 0 && c.a == 1)
			o.Albedo = lerp(c.rgb, _Color3, c.b);

		if (c.a == 0)
			o.Albedo = lerp(c.rgb, _Color4, 1-c.a);

		//o.Albedo = lerp(c.g, _Color2, c.g);
		//o.Albedo = lerp(c.b, _Color3, c.b);
		//o.Albedo = lerp(c.rgb, _Color4, 1-c.a);
	}
	ENDCG
	}
		Fallback "Diffuse"
}